"""
docstring
"""
def is_palindrome(input_word):
    """
    docstring
    """
    input_word = input_word.lower()
    length = len(input_word)
    if length < 2:
        return True
    if input_word[0] == input_word[length - 1]:
        return is_palindrome(input_word[1: length - 1])
    return False

if __name__ == "__main__":
    STRING = "AbraKadabra"
    rez = is_palindrome(STRING)

    if rez:
        print("Yes")
    else:
        print("No")
        